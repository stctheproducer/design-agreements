%% Start of file `awesome-agreement.cls'.
% Awesome Agreement Class File
%
% This class was based on Awesome-CV from:
% https://github.com/posquit0/Awesome-CV
%
% @Author: Chanda Mulenga <posquit0.bj@gmail.com>
% @Website: http://www.krvmedia.com
%
% Notes:
% 1) This class file defines the structure and layout of the template file (main.tex, resume.tex).
% 2) It has been written in such a way that under most circumstances you 
% should not need to edit it.
%
% Class license:
% LPPL v1.3c (http://www.latex-project.org/lppl)
%


%-------------------------------------------------------------------------------
%                Identification
%-------------------------------------------------------------------------------
\ProvidesClass{awesome-agreement}[2018/07/17 v1.0 Awesome Agreement Class]
\NeedsTeXFormat{LaTeX2e}


%-------------------------------------------------------------------------------
%                Class options
%
% (need to be done before the external package loading, for example because
% we need \paperwidth, \paperheight and \@ptsize to be defined before loading
% geometry and fancyhdr)
%-------------------------------------------------------------------------------
% Options for draft or final
\DeclareOption{draft}{\setlength\overfullrule{5pt}}
\DeclareOption{final}{\setlength\overfullrule{0pt}}
% Inherit options of article
\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{article}
}
\ProcessOptions\relax
\LoadClass{article}


%-------------------------------------------------------------------------------
%                3rd party packages
%-------------------------------------------------------------------------------
% Needed to make fixed length table
\RequirePackage{array}
% Needed to handle list environment
\RequirePackage{enumitem}
% Needed to handle text alignment
\RequirePackage{ragged2e}
% Needed to configure page layout
\RequirePackage{geometry}
% Needed to make header & footer effeciently
\RequirePackage{fancyhdr}
% Needed to manage colors
\RequirePackage{xcolor}
% Needed to use \ifxetex-\else-\fi statement
\RequirePackage{ifxetex}
% Needed to use \if-\then-\else statement
\RequirePackage{xifthen}
% Needed to use a toolbox of programming tools
\RequirePackage{etoolbox}
% Needed to change line spacing in specific environment
\RequirePackage{setspace}
% Needed to manage fonts
\ifxetex
  \RequirePackage[quiet]{fontspec}
  % To support LaTeX quoting style
  \defaultfontfeatures{Ligatures=TeX}
\else
  \RequirePackage[T1]{fontenc}
  % Replace by the encoding you are using
  \RequirePackage[utf8]{inputenc}
\fi
% Needed to manage math fonts
\RequirePackage[math-style=TeX]{unicode-math}
% Needed to use icons from font-awesome
% (https://github.com/posquit0/latex-fontawesome)
\RequirePackage{fontawesome}
% Needed to deal with paragraphs
\RequirePackage{parskip}
% Needed to deal hyperlink
\RequirePackage{hyperref}
\hypersetup{
  pdftitle={},
  pdfauthor={},
  pdfsubject={},
  pdfkeywords={},
  colorlinks=false,
  allbordercolors=white
}
% Needed for math calculations in variables
\RequirePackage{calc}
\RequirePackage{numprint}
% Needed for variables
\RequirePackage{variables}
% Needed to import images and control float positions
\RequirePackage{graphicx}
\RequirePackage{float}
% Needed for nice tables
\RequirePackage{tabularx}


%-------------------------------------------------------------------------------
%                Configuration for directory locations
%-------------------------------------------------------------------------------
% Configure a directory location for fonts(default: 'fonts/')
\newcommand*{\fontdir}[1][fonts/]{\def\@fontdir{#1}}
\fontdir


%-------------------------------------------------------------------------------
%                Configuration for layout
%-------------------------------------------------------------------------------
%% Page Layout
% Configure page margins with geometry
\geometry{margin=1in, footskip=.5cm}

%% Header & Footer
% Set offset to each header and offset
\fancyhfoffset{0em}
% Remove head rule
\renewcommand{\headrulewidth}{0pt}
% Clear all header & footer fields
\fancyhf{}
% Enable if you want to make header or footer using fancyhdr
\pagestyle{fancy}


%-------------------------------------------------------------------------------
%                Configuration for colors
%-------------------------------------------------------------------------------
% Gray-scale colors
\definecolor{white}{HTML}{FFFFFF}
\definecolor{black}{HTML}{000000}
\definecolor{darkgray}{HTML}{333333}
\definecolor{gray}{HTML}{5D5D5D}
\definecolor{lightgray}{HTML}{999999}
% Basic colors
\definecolor{green}{HTML}{C2E15F}
\definecolor{orange}{HTML}{FDA333}
\definecolor{purple}{HTML}{D3A4F9}
\definecolor{red}{HTML}{FB4485}
\definecolor{blue}{HTML}{6CE0F1}
% Text colors
\definecolor{darktext}{HTML}{414141}
\colorlet{text}{darkgray}
\colorlet{graytext}{gray}
\colorlet{lighttext}{lightgray}
% Awesome colors
\definecolor{awesome-emerald}{HTML}{00A388}
\definecolor{awesome-skyblue}{HTML}{0395DE}
\definecolor{awesome-red}{HTML}{DC3522}
\definecolor{awesome-pink}{HTML}{EF4089}
\definecolor{awesome-orange}{HTML}{FF6138}
\definecolor{awesome-nephritis}{HTML}{27AE60}
\definecolor{awesome-concrete}{HTML}{95A5A6}
\definecolor{awesome-darknight}{HTML}{131A28}
\colorlet{awesome}{awesome-red}

% Awesome section color
\newcounter{colorCounter}
\def\@sectioncolor#1#2#3{%
  {%
    \color{%
      \ifcase\value{colorCounter}%
        awesome\or%
        awesome\or%
        awesome\or%
        awesome\or%
        awesome\else%
        awesome\fi%
    } #1#2#3%
  }%
  \stepcounter{colorCounter}%
}


%-------------------------------------------------------------------------------
%                Configuration for fonts
%-------------------------------------------------------------------------------
% Set font for header (default is Roboto)
\newfontfamily\headerfont[
  Path=\@fontdir,
  UprightFont=*-Regular,
  ItalicFont=*-Italic,
  BoldFont=*-Bold,
  BoldItalicFont=*-BoldItalic,
]{Roboto}

\newfontfamily\headerfontlight[
  Path=\@fontdir,
  UprightFont=*-Thin,
  ItalicFont=*-ThinItalic,
  BoldFont=*-Medium,
  BoldItalicFont=*-MediumItalic,
]{Roboto}

% Set font for footer (default is Source Sans Pro)
\newfontfamily\footerfont[
  Path=\@fontdir,
  UprightFont=*-Regular,
  ItalicFont=*-Italic,
  BoldFont=*-Bold
]{Lato}

% Set font for body (default is Source Sans Pro)
\newfontfamily\bodyfont[
  Path=\@fontdir,
  UprightFont=*-Regular,
  ItalicFont=*-Italic,
  BoldFont=*-Bold,
  BoldItalicFont=*-BoldItalic
]{Lato}

\newfontfamily\bodyfontlight[
  Path=\@fontdir,
  UprightFont=*-Light,
  ItalicFont=*-LightItalic,
  BoldFont=*-Semibold,
  BoldItalicFont=*-SemiboldItalic
]{Lato}


%-------------------------------------------------------------------------------
%                Configuration for styles
%-------------------------------------------------------------------------------
% Configure styles for each CV elements
% For fundamental structures
\newcommand*{\companynamestyle}[1]{{\fontsize{32pt}{1em}\headerfont\bfseries\color{text} #1}}
\newcommand*{\nameofagreementstyle}[1]{{\fontsize{26pt}{1em}\headerfontlight\color{graytext} #1}}
\newcommand*{\headeraddressstyle}[1]{{\fontsize{8pt}{1em}\headerfont\itshape\color{lighttext} #1}}
\newcommand*{\headersocialstyle}[1]{{\fontsize{8pt}{1em}\headerfont\color{text} #1}}
\newcommand*{\headerquotestyle}[1]{{\fontsize{9pt}{1em}\bodyfont\itshape\color{darktext} #1}}
\newcommand*{\footerstyle}[1]{{\fontsize{8pt}{1em}\footerfont\scshape\color{lighttext} #1}}
\newcommand*{\sectionstyle}[1]{{\fontsize{16pt}{1em}\bodyfont\bfseries\color{text}\@sectioncolor #1}}
\newcommand*{\subsectionstyle}[1]{{\fontsize{12pt}{1em}\bodyfont\scshape\textcolor{text}{#1}}}
\newcommand{\agreementpar}[1]{\fontsize{10pt}{1em}\bodyfont\color{darkgray} #1}

%-------------------------------------------------------------------------------
%                Commands for extra
%-------------------------------------------------------------------------------
% Define separator for social informations in header
% Usage: \headersocialsep{<separator>}
% Default: \quad\textbar\quad
\newcommand*{\headersocialsep}[1][\quad\textbar\quad]{\def\@headersocialsep{#1}}
\headersocialsep


%-------------------------------------------------------------------------------
%                Commands for utilities
%-------------------------------------------------------------------------------
% Use to align an element of tabular table
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% Use to draw horizontal line with specific tickness
\def\vhrulefill#1{\leavevmode\leaders\hrule\@height#1\hfill \kern\z@}

% Use to execute conditional statements by checking empty string
\newcommand*{\ifempty}[3]{\ifthenelse{\isempty{#1}}{#2}{#3}}


%-------------------------------------------------------------------------------
%                Commands for elements of agreement structure
%-------------------------------------------------------------------------------
% Define a header for agreement
% Usage: \makeagreementheader
\newcommand*{\makeagreementheader}{
  \begin{titlepage}
    \vspace*{\fill}
    \begin{center}
      \line(1,0){450}
      \vspace{0.45in}
      \begin{figure}[H]
        \centering
        \includegraphics[height=2in]{/home/chanda/latex-projects/design-agreements/web-dev/images/Stand-Alone-Logo.png}
        \label{fig:companylogo}
      \end{figure}
      \\ \companynamestyle{
        \companyname
      } \\
      \vspace{2mm}
      \nameofagreementstyle{
        \nameofagreement
      } \\
      \vspace{2mm}
      \headersocialstyle{
        \newbool{isstart}
        \setbool{isstart}{true}
        \ifthenelse{\isundefined{\mobile}}
          {}
          {
            \faMobile\ \mobile
            \setbool{isstart}{false}
          }
        \ifthenelse{\isundefined{\landline}}
        {}
        {
          {\@headersocialsep}
          \faMobile\ \landline
          \setbool{isstart}{false}
        }
        \ifthenelse{\isundefined{\email}}
          {}
          {
            \ifbool{isstart}
              {
                \setbool{istart}{false} 
              }
              {\@headersocialsep}
            \href{mailto:\email}{\faEnvelope\ \email}
          }
        \ifthenelse{\isundefined{\website}}
          {}
          {
            \ifbool{isstart}
              {
                \setbool{istart}{false} 
              }
              {\@headersocialsep}
            \href{http://\website}{\faHome\ \website}
          }
        \ifthenelse{\isundefined{\github}}
          {}
          {
            \ifbool{isstart}
              {
                \setbool{istart}{false} 
              }
              {\@headersocialsep}
            \href{https://github.com/\github}{\faGithubSquare\ \github}
          }
        \ifthenelse{\isundefined{\twitter}}
          {}
          {
            \ifbool{isstart}
              {
                \setbool{istart}{false} 
              }
              {\@headersocialsep}
            \href{https://twitter.com/\twitter}{\faTwitter\ \twitter}
          }
      } \\
      \vspace{0.3in}
      \line(1,0){150}
    \end{center}
    \vfill
  \end{titlepage}
}

% Define a footer for agreement
% Usage: \makeagreementfooter{<left>}{<center>}{<right>}
\newcommand*{\makeagreementfooter}[3]{
  \fancyfoot{}
  \fancyfoot[L]{
    \footerstyle{#1}
  }
  \fancyfoot[C]{
    \footerstyle{#2}
  }
  \fancyfoot[R]{
    \footerstyle{#3}
  } 
}

% Define a section for agreement
% Usage: \agreementsection{<section-title>}
\newcommand{\agreementsection}[1]{
  \par\addvspace{1.5ex}
  \phantomsection{}
  \sectionstyle{#1}
  \color{gray} % \vhrulefill{0.9pt}
  \par\nobreak\addvspace{1ex}
}

% Define a subsection for agreement
% Usage: \agreementsubsection{<subsection-title>}
\newcommand{\agreementsubsection}[1]{
  \phantomsection{}
  \subsectionstyle{#1}
}